#include <stdlib.h>
#include <stdatomic.h>
#include <mr.h>

#define CACHE_LINE_SIZE 64

struct global_data {
	//global stuff
};

struct local_data {
	//local stuff
	atomic_flag claim;
};

union mr_entry {
	struct global_data global;
	struct local_data tls;
} __attribute__((aligned(CACHE_LINE_SIZE)));

union mr_entry *init_mr(int max_threads)
{
	union mr_entry *array = aligned_alloc(
			CACHE_LINE_SIZE,
			(max_threads) * sizeof(union mr_entry));
	for(int i=0; i<max_threads; i++){
		atomic_flag_clear(&(array[i].tls.claim));
	}
	return array;
}

int mr_thread_acquire(
		union mr_entry *array,
		int max_threads)
{
	int i = 0;
	while(1){
		if(!atomic_flag_test_and_set(&(array[i].tls.claim)))
			return i;
		i = (i+1) % max_threads;
	}
}

void mr_thread_release(
		union mr_entry *array,
		int thread_id)
{
	atomic_flag_clear(&(array[thread_id].tls.claim));
}
